# -*- coding: utf-8 -*-

from django import forms
from django.forms import Form
from django.contrib.auth.models import User

from django.template import loader
from django.contrib.auth.forms import *


class CustomPwdResetForm(PasswordResetForm):

    def save(self, domain_override=None,
             subject_template_name='registration/password_reset_subject.txt',
             email_template_name='accounts/password_reset_email.html',
             use_https=False, token_generator=default_token_generator,
             from_email=None, request=None):
        """
        Generates a one-use only link for resetting password and sends to the
        user.
        """
        for user in self.users_cache:
            if not domain_override:
                current_site = get_current_site(request)
                site_name = current_site.name
                domain = current_site.domain
            else:
                site_name = domain = domain_override
            c = {
                'email': user.email,
                'domain': domain,
                'site_name': site_name,
                'uid': int_to_base36(user.id),
                'user': user,
                'token': token_generator.make_token(user),
                'protocol': use_https and 'https' or 'http',
            }
            subject = loader.render_to_string(subject_template_name, c)
            # Email subject *must not* contain newlines
            subject = ''.join(subject.splitlines())
            
            message_html = loader.render_to_string(email_template_name, c)
            message_text = loader.render_to_string('accounts/password_reset_email.txt', c)
            
            from django.core.mail import EmailMultiAlternatives

            msg = EmailMultiAlternatives(subject, message_text, from_email, [user.email])
            msg.attach_alternative(message_html, "text/html")
            msg.send()


class SignUpForm(Form):
    error_css_class = 'error'
    required_css_class = 'required'

    username = forms.CharField(label="Nombre", error_messages={'required':'Tienes que ingresar tu nombre'})
    email = forms.EmailField(label="E-mail", error_messages={'required':'Tienes que ingresar tu e-mail', 'invalid':'Tienes que ingresar un e-mail valido'})
    password = forms.CharField(widget=forms.PasswordInput, label="Contraseña", error_messages={'required':'Tienes que ingresar tu contraseña'})
    verify = forms.CharField(widget=forms.PasswordInput, label="Verifica contraseña", error_messages={'required':'Tienes que ingresar tu contraseña'})

    def clean(self):
        cleaned_data = super(SignUpForm, self).clean()
        password = cleaned_data.get('password')
        verify = cleaned_data.get('verify')

        if verify and password != verify:
            self._errors['verify'] = self.error_class([u'Las 2 contraseñas deben ser identicas'])
            del cleaned_data['verify']

        return cleaned_data

    
    def clean_field(self, field, error):
        data = self.cleaned_data[field]

        if User.objects.filter(**{field:data}).exists():
            raise forms.ValidationError(error % (data))

        return data


    def clean_username(self):
        return self.clean_field('username', "El nombre de usuario '%s' ya esta tomado. Elige otro")


    def clean_email(self):
        return self.clean_field('email', "El email '%s' ya fue registrado!")


class ProfileForm(Form):
    error_css_class = 'error'
    required_css_class = 'required'

    name = forms.CharField(label="Nombre", required=False)
    last_name = forms.CharField(label="Apellido", required=False)
    email = forms.EmailField(label="E-mail", error_messages={'required':'Tienes que ingresar tu e-mail', 'invalid':'Tienes que ingresar un e-mail valido'})
    send_mails = forms.BooleanField(label='Contactarme via e-mail', required=False)

    def __init__(self, user, request=None):
        super(ProfileForm, self).__init__(request)
        self._user = user

    def clean(self):
        cleaned_data = super(ProfileForm, self).clean()
        return cleaned_data

    def clean_email(self):
        data = self.cleaned_data['email']

        if User.objects.filter(**{'email':data}).exists():
            if self._user.email != data:
                raise forms.ValidationError("El email '%s' ya fue registrado!" % (data))

        return data


class PasswordForm(Form):
    error_css_class = 'error'
    required_css_class = 'required'

    password = forms.CharField(widget=forms.PasswordInput, label="Contraseña", required=False)
    verify = forms.CharField(widget=forms.PasswordInput, label="Verifica contraseña", required=False)

    def __init__(self, user, request=None):
        super(PasswordForm, self).__init__(request)
        self._user = user

    def clean(self):
        cleaned_data = super(PasswordForm, self).clean()
        password = cleaned_data.get('password')
        verify = cleaned_data.get('verify')

        if verify and password != verify:
            self._errors['verify'] = self.error_class([u'Las 2 contraseñas deben ser identicas'])
            del cleaned_data['verify']

        return cleaned_data


class FeedbackForm(Form):
    error_css_class = 'error'
    required_css_class = 'required'

    message = forms.CharField(widget=forms.Textarea, label='Mensaje', 
        error_messages={'required':'Tienes que escribir un mensaje'})


