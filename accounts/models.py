from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save

from updatedb.models import Info


class UserProfile(models.Model):
    user = models.OneToOneField(User)
    wish_list = models.ManyToManyField(Info)
    send_mails = models.BooleanField(default=True)
    

def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)

post_save.connect(create_user_profile, sender=User)


