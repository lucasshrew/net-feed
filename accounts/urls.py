from django.conf.urls.defaults import patterns, url
from django.views.generic import TemplateView

from accounts.forms import CustomPwdResetForm

urlpatterns = patterns('django.contrib.auth.views',
    url(r'^logout/$', 'logout', {'next_page': '/'}),
    url(r'^password/reset/$', 'password_reset', 
        {
            'template_name': 'accounts/password_reset_form.html', 
            'password_reset_form': CustomPwdResetForm,
            'email_template_name': 'accounts/password_reset_email.html' 
        }),
    url(r'^password/reset/confirm/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$', 
        'password_reset_confirm', {'template_name':'accounts/password_reset_confirm.html'}),
    url(r'^password/reset/done/$', 'password_reset_done', 
        {'template_name':'accounts/password_reset_done.html'}),
    url(r'^password/reset/complete/$', 'password_reset_complete', 
        {'template_name':'accounts/password_reset_complete.html'}),
)

# signup flow
urlpatterns += patterns('accounts.views',
    url(r'^login/$', 'login', {'template_name': 'accounts/login.html'}),
    url(r'^signup/$', 'signup'),
    url(r'^mailsent/$', TemplateView.as_view(template_name='accounts/mailsent.html')),
    url(r'^signup/email/confirm/(?P<uidb36>[0-9A-Za-z]+)/(?P<token>.+)/$', 'signup_confirm'),
    url(r'^signup/complete/$', TemplateView.as_view(template_name='accounts/signup_complete.html')),
    url(r'^signup/error/$', TemplateView.as_view(template_name='accounts/signup_error.html')),
    url(r'^profile/$', 'profile'),
    url(r'^mailupdated/$', TemplateView.as_view(template_name='accounts/mailupdated.html')),
    url(r'^close/$', 'close_account'),
    url(r'^close/done/$', TemplateView.as_view(template_name='accounts/closed.html')),
    url(r'^login/app/$', 'app_login', {'template_name': 'accounts/app_login.html'}),
)

