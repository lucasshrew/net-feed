from django.shortcuts import render, render_to_response, get_object_or_404
from accounts import forms
from django.contrib.auth.models import User
from django.contrib.auth.views import login as contrib_login
from django.http import HttpResponseRedirect, Http404
from django.contrib.auth import logout

from django.template import loader, RequestContext
from django.contrib.sites.models import get_current_site
from django.contrib.auth.tokens import default_token_generator
from django.utils.http import int_to_base36, base36_to_int
from django.core.mail import EmailMultiAlternatives

from django.contrib.auth.decorators import login_required

from homepage.views import get_duedate_content

from settings import EMAIL_HOST_USER

def signup(request):
    if request.method == "POST":
        form = forms.SignUpForm(request.POST)
        if form.is_valid():
            cleaned_data = form.cleaned_data
            
            user = User.objects.create_user(cleaned_data['username'], cleaned_data['email'], cleaned_data['password'])
            user.is_active = False
            
            send_confirm_email_msg(request, user)
            
            user.save()

            return HttpResponseRedirect('/accounts/mailsent/')
    else:
        form = forms.SignUpForm()

    return render(request, 'accounts/signup.html', {'form':form})


def send_confirm_email_msg(request, user):
    current_site = get_current_site(request)
    site_name = current_site.name
    domain = current_site.domain
    
    c = {
        'email': user.email,
        'domain': domain,
        'site_name': site_name,
        'uid': int_to_base36(user.id),
        'user': user,
        'token': default_token_generator.make_token(user),
        'protocol': 'http',
    }
    subject = loader.render_to_string('accounts/confirm_email_msg_subject.txt', c)

    # Email subject *must not* contain newlines
    subject = ''.join(subject.splitlines())
    
    message_html = loader.render_to_string('accounts/confirm_email_msg_content.html', c)
    message_text = loader.render_to_string('accounts/confirm_email_msg_content.txt', c)
    
    msg = EmailMultiAlternatives(subject, message_text, EMAIL_HOST_USER, [user.email])
    msg.attach_alternative(message_html, "text/html")
    msg.send()


def signup_confirm(request, uidb36=None, token=None):
    assert uidb36 is not None and token is not None
    
    try:
        uid_int = base36_to_int(uidb36)
    except ValueError:
        raise Http404

    user = get_object_or_404(User, id=uid_int)

    if default_token_generator.check_token(user, token):
        user.is_active = True
        user.save()
        return HttpResponseRedirect('/accounts/signup/complete/')

    return HttpResponseRedirect('/accounts/signup/error/')


def login(request, template_name):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/')
    else:
        return contrib_login(request, **{'template_name':template_name})


@login_required
def profile(request):
    user = request.user

    if request.method == "POST":
        if 'profile_update' in request.POST:
            form = forms.ProfileForm(user, request.POST)
            form_pwd = forms.PasswordForm(user)
            if form.is_valid():
                cleaned_data = form.cleaned_data
                
                user.first_name = cleaned_data['name']
                user.last_name = cleaned_data['last_name']
                user.get_profile().send_mails = cleaned_data['send_mails']
                
                if user.email != cleaned_data['email']:
                    user.email = cleaned_data['email']
                    send_confirm_email_msg(request, user)
                    user.save()
                    return HttpResponseRedirect('/accounts/mailupdated/')
                
                user.save()
       
        elif 'pwd_update' in request.POST:
            form = forms.ProfileForm(user)
            form_pwd = forms.PasswordForm(user, request.POST)
            if form_pwd.is_valid():
                cleaned_data = form_pwd.cleaned_data
                
                if cleaned_data['verify'].strip() != '':
                    user.set_password(cleaned_data['verify'])
                    user.save()

    else:
        form = forms.ProfileForm(user)
        form_pwd = forms.PasswordForm(user)
    
    form.fields['email'].initial  = user.email
    form.fields['name'].initial  = user.first_name
    form.fields['last_name'].initial  = user.last_name
    form.fields['send_mails'].initial  = user.get_profile().send_mails

    return render(request, 'accounts/profile.html', {'form':form, 'pwd_form':form_pwd,
        'due_dates_count': get_duedate_content()['count']
        }, 
        context_instance=RequestContext(request))


@login_required
def close_account(request):
    user = request.user

    if request.method == 'POST':
        form = forms.FeedbackForm(request.POST)
        
        if form.is_valid():
            name = user.username
            message = form.cleaned_data['message']
            sender = user.email

            subject = 'Closed account from: %s <%s>' % (name, sender)
            recipients = ['feedback@net-feeds.com']
            
            try:
                from django.core.mail import send_mail
                send_mail(subject, message, sender, recipients)
            except:
                pass
            
        logout(request)
        user.delete()
        return HttpResponseRedirect('/accounts/close/done/')
    else:
        form = forms.FeedbackForm()

    return render(request, 'accounts/close.html', {'form': form,})


def app_login(request, template_name):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/wishlist/app/')
    else:
        return contrib_login(request, **{'template_name':template_name})


