#! /usr/bin/python2.6
# coding: utf-8

from __future__ import with_statement

import os
import signal
import logging

from configs import URL
from configs import LOGGER

LOGGER.DEFAULT_LEVEL = logging.DEBUG
LOGGER.USE_STDOUT = True
LOGGER.FILE = 'bot.log'

from log import logger

import browser
import soup
import db

import shutil
import datetime

def signal_handler(signum, frame):
    logger.error("=== Arrived SIGNAL %d ===", signum)
    if signum in (1, 2, 3, 15, 9):
        logger.error("The process received a killing SIGNAL")


for i in range(32):
    try:
        signal.signal(i, signal_handler)
    except (RuntimeError, ValueError):
        logger.debug("Skipping %s", i)


def generate():
    """Couldn't find a better name :)"""
    
    if not os.path.exists(LOGGER.ERROR_DIR):
        os.mkdir(LOGGER.ERROR_DIR)

    browser.login()
    browser.go_to('http://www.netflix.com/browse')

    # link_bag will have one entry for each Genre link
    link_bag = set()
    genre_link_bag = set()

    with open('debug.html', 'w') as f:
        f.write(browser.get_page())

    # Adults movies URLs
    for i in xrange(100):
        try:
            alink = browser.get_link(url='WiGenre', index=i)
            logger.info('Genre Link: %s', str(alink))
            genre_link_bag.add(alink.replace('WiGenre', 'WiAltGenre'))
        except browser.LinkNotFoundError:
            logger.debug('Links limit: %s', str(i))
            break

    # Adults movies URLs
    for i in xrange(100):
        try:
            alink = browser.get_link(url='WiAltGenre', index=i)
            logger.info('Genre Link: %s', str(alink))
            genre_link_bag.add(alink)
        except browser.LinkNotFoundError:
            logger.debug('Links limit: %s', str(i))
            break

    # Kids movies URLs
    for i in xrange(100):
        try:
            alink = browser.get_link(url='KidsAltGenre', index=i)
            logger.info('Genre Link: %s', str(alink))
            link_bag.add(alink)
        except browser.LinkNotFoundError:
            logger.debug('Links limit: %s', str(i))
            break

    # HD movies URL
    link_bag.add('http://movies.netflix.com/WiHD?')
    link_bag.add('http://movies.netflix.com/WiHome')

    # We fecth each page and save it in 'out' folder
    if os.path.exists('out'):
        d = datetime.date.today()
        shutil.move('out', 'out-%d-%d-%d'%(d.year, d.month, d.day))

    os.mkdir('out')

    def download_links(link_bag, step=0, params='&vt=tg&ftr=false&pn=0&np=30', outname=''):
        count = 0
        for i, link in enumerate(link_bag):
            if '?' in link:
                browser.go_to(link+params)
            else:
                browser.go_to(link)
            filename = 'out/%s%d.html'% (outname, i+step)
            
            with open(filename, 'w') as out:
                out.write(browser.get_page())

            count = i

        return count + 1

    def download_links_from_json(link_bag, step=0, params='&vt=tg&ftr=false&pn=0&np=30&actionMethod=json', outname=''):
        count = 0
        for i, link in enumerate(link_bag):
            if '?' in link:
                browser.go_to(link + params)
            else:
                browser.go_to(link + '?' + params)
            
            filename = 'out/%s%d.html'% (outname, i+step)
            with open(filename, 'w') as out:
                try:
                    content = browser.get_json()
                    html = content.get(u'html', content)
                    out.write(html)
                except browser.NavigationError, e:
                    logger.error(e)
                    logger.error('Error content on file %s', filename)
                    out.write(browser.get_page())

            count = i

        return count + 1

   # download voted movies
   # browser.go_to(URL.ACCOUNT_VOTES)
   # voted_links = soup.get_pagination_links(browser.get_page())
   # download_links(voted_links, outname='voted', params='')

    # download adult genre links
    step = download_links_from_json(genre_link_bag)
    more_links = soup.get_genre_links('out')
    step += download_links_from_json(more_links, step)
    
    # download rest of genre links
    step += download_links(link_bag, step)
    more_links = soup.get_genre_links('out')
    download_links(more_links, step)
    
    logger.debug('number of pages to process: %s', step)
    
    # download user history
    download_links([URL.ACCOUNT_ACTIVITY], outname='history', params='')

    logger.info("deleting not needed data")
    del link_bag
    del genre_link_bag
    #del voted_links
    del more_links

    logger.info("Generating database from out...")
    added = soup.dump_database('out')
    logger.info("Added new ids are: %d %s", len(added), str(added))

    repeated_ids = db.check_info_uniqueness()
    if repeated_ids:
        logger.debug("Repeated Info's UID:")
        for k, c in repeated_ids:
            logger.warn("uid: %s count: %d", str(k), c)

    logger.info("Bot finishde OK.")


def get_series():
    try:
        return db.get_series('out')
    except IOError:
        logger.warn("Database not ready.. Downloading.")
        generate()
        return get_series()


if __name__ == '__main__':
    pid = os.getpid()
    pidfile = open('bot.pid', 'w')
    pidfile.write(str(pid))
    pidfile.close()
    generate()
    os.remove('bot.pid')

