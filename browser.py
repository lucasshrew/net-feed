# coding: utf-8

"""
Wraps a zope.testbrowser

This module will retain the same instance of the browser, so you don't have to
care about having the correct browser session.
"""

import os
import time
import requests
import re
import json

from urllib import urlencode
from BeautifulSoup import BeautifulSoup

from log import logger
from configs import URL


class NavigationError(Exception):
    def __init__(self, error=None, url=None):
        super(Exception, self).__init__()
        self.url = url
        self.error = error

    def __str__(self):
        return "Error %s, when navigating to: %s."% (self.error, self.url)


class LinkNotFoundError(Exception):
    def __init__(self, url=None, index=0):
        super(Exception, self).__init__()
        self.url = url
        self.index = str(index)

    def __str__(self):
        return "Link index %s not found for: %s."% (self.index, self.url)


# Global state variables
_session = None
_logged = False
_last_response = None

count = 0
def get_new_session():
    global count
    
    user = os.environ['NETFLIX_USER']
    pwd = os.environ['NETFLIX_PWD']
    if count%2 == 0:
        user = os.environ['NETFLIX_USER_2']
        pwd = os.environ['NETFLIX_PWD_2']

    s = requests.Session()
    s.auth = (user, pwd)
    #s.headers.update({'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.115 Safari/537.36'})
    s.headers.update({'User-Agent':'Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_2_1 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8C148 Safari/6533.18.5'})
    s.headers.update({'Connection': 'keep-alive'})
    s.headers.update({'Host': 'www.netflix.com'})
    s.headers.update({'Connection': 'keep-alive'})
    count += 1
    return s


def login_to_session():
    global _session, _last_response

    _session = get_new_session()
    
    user = os.environ['NETFLIX_USER']
    pwd = os.environ['NETFLIX_PWD']
    if count%2 == 0:
        user = os.environ['NETFLIX_USER_2']
        pwd = os.environ['NETFLIX_PWD_2']
    
    query = re.compile('name=\"authURL\"\ value=\"(.*)\"')

    try:
        form_response = _session.get(URL.LOGIN)
        auth_url = query.findall(form_response.content)[0]

        data = {}
        data['email'] = user
        data['password'] = pwd
        data['authURL'] = auth_url

        login_response = _session.post(URL.LOGIN, data)
        #home_response = _session.get(URL.WIHOME)
        _last_response = login_response

        status = form_response.status_code == 200
        status = status and login_response.status_code == 200
        #status = status and home_response.status_code == 200

        return status
    except IndexError:
        logger.warn("Already logged in or the page change its structure.")


def go_to(url, retries=4, need_login=True):
    """Opens the given url"""
    global _last_response

    time.sleep(5)

    logger.debug('Attempt to open: %s', url)
    need_to_retry = False
    error = ""
    try:
        if not is_logged() and need_login:
            login()

        if need_login:
            response = _session.get(url)
        else:
            response = requests.get(url)

        logger.info('Opened: %s with status: %d', url, response.status_code)

        if is_error_page(response.content):
            logger.info('Netflix service error: %s', url)
            need_to_retry = True
        else:
            _last_response = response

    except Exception, e:
        logger.exception('Error navigating to: %s', url)
        need_to_retry = True
        error = e

    if need_to_retry or _last_response.status_code != 200:
        if retries > 0:
            retries -= 1
            cleanup()
            time.sleep(60)
            logger.info('re-trying...')
            go_to(url, retries, need_login)
        else:
            raise NavigationError(error, url)


def login():
    """Logins to a Netflix account.
       
       It retrieves the user settings from two environment variables:
       NETFLIX_USER and NETFLIX_PWD.
    """
    global _logged

    try:
        _logged = login_to_session()
    except:
        logger.exception('Error ocurred during login to Netflix account.')
        try:
            time.sleep(60)
            logger.info('re-trying login')
            _logged = login_to_session()
        except Exception, e:
            raise NavigationError(e, 'Login URL')
        

def get_page(url=None):
    """returns the current page in the browser as a string."""
    if url is not None:
        go_to(url)
    return _last_response.content


def get_json(url=None):
    """returns the current page in the browser as a string."""
    if url is not None:
        go_to(url)
    try:
        return json.loads(_last_response.content)
    except ValueError, e:
        raise NavigationError(e, url)


def get_all_links(url):
    soup = BeautifulSoup(_last_response.content)
    return [ tag['href'] for tag in soup.findAll('a', href=re.compile(url)) ]


def get_link(url, index=0):
    """ get_link(url[, index=0) -> zope.testbrowser.interfaces.ILink
    
        Searches the current page for the given url and returns an ILink object.
    """
    logger.debug('Attempt to get link: %s, index %d', url, index)
    links = get_all_links(url)

    if index >= len(links):
        raise LinkNotFoundError(url, index)
    return links[index]


def post(url, body={}):
    return _session.post(url, body)


def is_logged():
    """ is_logged() -> bool

        Returns True if the browser is logged to NetFlix account.
    """
    return _logged


def cleanup():
    """ Performs a full cleanup of browser session
    """
    global _session, _logged, _last_response
    _session = None
    _logged = False
    _last_response = None


def is_error_page(lines):
    if lines.find('siteProblemsPostcard') != -1:
        return True
    return False

