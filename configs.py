import logging


class URL:
    LOGIN = 'https://signup.netflix.com/Login'
    WIHOME = 'http://www.netflix.com/WiHome'
    WIMOVIE = 'http://movies.netflix.com/WiMovie/'
    ACCOUNT_ACTIVITY = 'https://account.netflix.com/WiViewingActivity?all=true'
    ACCOUNT_VOTES = 'http://movies.netflix.com/MoviesYouveSeen'
    METADATA = 'http://api-global.netflix.com/desktop/odp/metadata?video=%s'
    SERIE_INFO = 'http://api-global.netflix.com/desktop/odp/episodes?video=%s'

class LOGGER:
    USE_STDOUT = False
    FILE = 'server.log'
    DEFAULT_LEVEL = logging.ERROR
    MAX_BYTES = 1024 * 1024 * 4
    BACKUP_COUNT = 10
    ERROR_FILE = 'failed_ids.log'
    ERROR_DIR = 'errors_pages'
