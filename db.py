#Code to make django work
import os
import sys
path = os.getcwd().split(os.sep)
sys.path.append(os.sep.join(path[:-1]))
os.environ['DJANGO_SETTINGS_MODULE'] = path[-1]+'.settings'
#End django initialization here

import django.db
from django.db.models import Q

import itertools
from updatedb.models import Movie, Info, Serie, Season, Episode

from log import logger
from datetime import datetime


def clear_cache():
    django.db.reset_queries()


def get_series_episodes():
    return get_query(Episode.objects.all())


def get_series():
    return get_query(Serie.objects.all())


def get_movies():
    return get_query(Movie.objects.all())


def get_infos():
    return Info.objects.values_list('uid')


def get_main_content_infos():
    q = Q(movie__isnull=False)
    q = q | Q(serie__isnull=False)
    q = q | Q(season__isnull=False)

    return Info.objects.filter(q).values_list('uid')


def get_query(Q):
    ids = []
    for o in Q:
        ids.append(o.info.uid)

    return ids


def create_movie(info, pic_url):
    info['timestamp'] = datetime.today()
    i = Info(**info)
    i.save()
    c = Movie(info=i, pic_url=pic_url)
    c.save()


def create_serie(info, pic_url):
    uid = info.pop('uid')
    info['timestamp'] = datetime.today()
    info, created  = Info.objects.get_or_create(uid=uid, defaults=info)
    if created:
        info.save()
    
    serie, created  = Serie.objects.get_or_create(info=info, pic_url=pic_url)    
    if created:
        serie.save()

    return serie.pk


def add_season_to_serie(info, serie_pk, force_time=False):
    """Adds the season to the serie and returns the season id"""

    serie = Serie.objects.get(pk=serie_pk)
    
    # Create info if not exists
    uid = info.pop('uid')

    if force_time:
        info['timestamp'] = serie.info.timestamp
    else:
        info['timestamp'] = datetime.today()

    info, created = Info.objects.get_or_create(uid=uid, defaults=info)
    if created:
        info.save()

    # Create a Season if not exists
    #season, created = Season.objects.get_or_create(info=info, serie=serie)
    #if created:
    #    season.save()

    # Add Season to serie and return PK
    season, _ = serie.season_set.get_or_create(info=info)
    serie.save()
    season.save()
    return season.pk


def add_episode_to_season(number, info, season_pk, force_time=False):
    season = Season.objects.get(pk=season_pk)

    uid = info.pop('uid')
    
    if force_time:
        info['timestamp'] = season.info.timestamp
    else:
        info['timestamp'] = datetime.today()
    
    info, created = Info.objects.get_or_create(uid=uid, defaults=info)
    if created:
        info.save()
    
    episode, created = Episode.objects.get_or_create(info=info, defaults={'number':number})
    if not created:
        episode.number = number
    episode.save()

    season.episode_set.add(episode)
    season.save()

    return episode.pk


def get_serie_pk(uid):
    series = Serie.objects.filter(info__uid=uid)
    if series:
        return series[0].pk
    return None


def get_seasons_of_serie(serie_pk):
    seasons = Season.objects.filter(serie=serie_pk).values('info__uid', 'id')
    seasons_uids = dict((x['info__uid'], x['id']) for x in seasons)
    return seasons_uids

    
def get_episodes_of_season(season_pk):
    episodes = Episode.objects.filter(season=season_pk).values('info__uid', 'id')
    episodes_uid = dict((x['info__uid'], x['id']) for x in episodes)
    return episodes_uid


def update_info_due_date(uid, date):
    infos = Info.objects.filter(uid=uid)

    updated = False
    for i in infos:
        if i.due_date != date:
            i.due_date = date
            i.save()
            updated = True
    return updated


def update_info_orig_title(uid, title):
    infos = Info.objects.filter(uid=uid)

    updated = False
    for i in infos:
        if i.orig_title != title:
            i.orig_title = title
            i.save()
            updated = True
    return updated


def check_info_uniqueness():
    all = Info.objects.all().order_by('uid')
    grouped = itertools.groupby(all, lambda x: x.uid)

    repeated = []
    for k, v in grouped:
        c = sum(1 for _ in v)
        if c > 1:
            repeated.append((k, c))

    return repeated


def remove_duplicate_infos():
    logger.info('Cleaning up duplicate infos')
    repeated = check_info_uniqueness()

    logger.info(repeated)
    
    for id, count in repeated:
        infos = Info.objects.filter(uid=id).order_by('id')
        for i in infos[:len(infos)-1]:
            logger.info('removing info: %s', i)
            i.delete()
    repeated = check_info_uniqueness()
    logger.info('cleaned up all repeated dara: %s', repeated)

