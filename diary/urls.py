from django.conf.urls.defaults import patterns, url

urlpatterns = patterns('diary.views',
            url(r'^(?P<year>\d+)/(?P<month>\d+)$', 'show'),
)
