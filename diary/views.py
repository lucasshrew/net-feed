from datetime import date
import datetime
from django.db.models import Min

from updatedb.models import Info
from django.shortcuts import render_to_response
from homepage.views import get_diary

def show(request, year, month):
    day = datetime.date(int(year), int(month), datetime.date.today().day)
    day0 = Info.objects.aggregate(Min('timestamp'))['timestamp__min']
    info_list = Info.objects.all().order_by('-timestamp')
    return render_to_response('diary/calendar.html', {'diary': get_diary(info_list.exclude(timestamp=day0), day), 'date':date(int(year), int(month), datetime.date.today().day), 'today': date.today()})

