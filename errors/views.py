import os, settings
from django.shortcuts import render_to_response

def index(req):
    title = 'Bot finished succesfully'
    pid = None
    fname = settings.PROJECT_PATH+os.sep+'bot.pid';
    if os.path.exists(fname):
        f = open(fname)
        pid = f.read()

        title = 'Bot finished wrongly!'

        for proc in os.popen('ps ax | grep bot.py'):
            if pid in proc:
                title = 'Bot is running... pid: ' + str(pid)

    log_file = open(settings.PROJECT_PATH+os.sep+'server.log')
    p = os.getcwd()

    return render_to_response('errors/errors.html', {'title':title, 'log':log_file, 'pwd':p})
