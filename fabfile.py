from __future__ import with_statement

from fabric.api import env, run, require, task, hide, show
from fabric.tasks import execute
from fabric.context_managers import cd  

#env.hosts = ['net-feeds.com:4198']
env.hosts = ['179.43.125.132:5785']
env.user = 'netfeed'
env.password = 'netflix12arg'

config = {}

@task
def production():
    ''' Set production app as current deploy target.'''

    env.fab_hosts = ['net-feeds.com']
    env.repos = (['netfeeds', 'origin', 'master'],)
    config['app'] = 'netfeeds'
    config['django_prj'] = 'netfeeds'

@task
def testing():
    ''' Set testing app as current deploy target.'''

    env.fab_hosts = ['net-feeds.com']
    env.repos = (['netfeeds', 'origin', 'develop'],)
    config['app'] = 'testing'
    config['django_prj'] = 'netfeeds'


@task
def deploy():
    ''' Performs a full deploy of the current target.'''

    require('fab_hosts', provided_by=[production, testing])
    
    with hide('running', 'stdout'):
        execute(pull)
        execute(copy_statics)
    execute(restart_server)


@task(default=True)
def deploy_production():
    ''' Performs a production full deploy.'''
    execute(production)
    execute(deploy)


def git_pull():
    '''Performs a git pull.'''
    with show('running', 'stdout'):
        path = '~/webapp/%(repo)s'% config
        with cd(path):
            print('Updating working copy: %s'% path)
            run('git pull %(parent)s %(branch)s'% config)


@task(alias='rst')
def restart_server():
    '''Restarts Apache2 server.'''

    require('fab_hosts', provided_by=[production, testing])
    run("sudo service apache2 restart")


@task
def pull():
    ''' Performs a git pull in every remote working copy. '''

    require('fab_hosts', provided_by=[production, testing])
    for repo, parent, branch in env.repos:
        config['repo'] = repo
        config['parent'] = parent
        config['branch'] = branch
        execute(git_pull)


def copy_statics():
    ''' Copy static files to apache www dir.'''
    # in our new environment we don't need to copy anything
    pass
    #with cd('~/webapps/%(app)s/%(django_prj)s/static'% config):
    #    run('cp -f -r * ~/webapps/apache/')


@task
def clean_cache():
    ''' Cleans django cache '''
    require('fab_hosts', provided_by=[production, testing])
    for repo, parent, branch in env.repos:
        config['repo'] = repo
        config['parent'] = parent
        config['branch'] = branch

        with show('running', 'stdout'):
            path = '~/webapp/%(repo)s/cache'% config
            with cd(path):
                print('Cleaning cache: %s'% path)
                run('rm -rf *')

