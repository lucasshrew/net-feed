from django import forms

class ContactForm(forms.Form):
    error_css_class = 'error'
    required_css_class = 'required'

    username = forms.CharField(max_length=100, label='Nombre', 
        error_messages={'required':'Tienes que ingresar tu nombre'})
    
    sender = forms.EmailField(label='Direccion de e-mail', 
                              error_messages={'required':'Tienes que ingresar tu e-mail', 'invalid':'El e-mail debe ser valido;'})

    message = forms.CharField(widget=forms.Textarea, label='Mensaje', 
        error_messages={'required':'Tienes que escribir un mensaje'})

