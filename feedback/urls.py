from django.conf.urls.defaults import patterns, url
from django.views.generic import TemplateView

urlpatterns = patterns('feedback.views',
    url(r'^$', 'index'),
    url(r'^thanks/$', TemplateView.as_view(template_name='feedback/thanks.html')),
    url(r'^failed/$', TemplateView.as_view(template_name='feedback/failed.html')),
)

