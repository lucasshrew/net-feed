from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.shortcuts import render_to_response

from forms import ContactForm

def index(request):
    if request.method == 'POST': # If the form has been submitted...
        form = ContactForm(request.POST) # A form bound to the POST data
        
        if form.is_valid(): # All validation rules pass
            # Process the data in form.cleaned_data
            name = form.cleaned_data['username']
            message = form.cleaned_data['message']
            sender = form.cleaned_data['sender']

            subject = 'Feedback from: %s <%s>' % (name, sender)
            recipients = ['feedback@net-feeds.com']

            try:
                from django.core.mail import send_mail
                send_mail(subject, message, sender, recipients)
            except:
                return HttpResponseRedirect('/feedback/failed') # Redirect after POST
            else:
                return HttpResponseRedirect('/feedback/thanks') # Redirect after POST
    else:
        form = ContactForm() # An unbound form

    return render(request, 'feedback/feedback.html', {
        'form': form,
    })

