# -*- coding: utf-8 -*-

from django.template.loader import render_to_string
from django.contrib.syndication.views import Feed
from django.utils.feedgenerator import Rss201rev2Feed

from django.db.models import Q, Min
import datetime

from updatedb import models
from updatedb.models import Info

from utils.formatters import QuantityExpression
from utils.formatters import HumanEnumeration

class ExtendedRSSFeed(Rss201rev2Feed):
    """
    Create a type of RSS feed that has content:encoded elements.
    """
    def rss_attributes(self):
        attrs = super(ExtendedRSSFeed, self).rss_attributes()
        attrs[u'xmlns:content'] = u'http://purl.org/rss/1.0/modules/content/'
	attrs[u'xmlns:wfw'] = u'http://wellformedweb.org/CommentAPI/'
	attrs[u'xmlns:dc'] = u'http://purl.org/dc/elements/1.1/'
	attrs[u'xmlns:atom'] = u'http://www.w3.org/2005/Atom'
	attrs[u'xmlns:sy'] = u'http://purl.org/rss/1.0/modules/syndication/'
	attrs[u'xmlns:slash'] = u'http://purl.org/rss/1.0/modules/slash/'
        return attrs

    def root_attributes(self):
        attrs = super(ExtendedRSSFeed, self).root_attributes()
        return attrs
        
    def add_item_elements(self, handler, item):
        super(ExtendedRSSFeed, self).add_item_elements(handler, item)
        handler.addQuickElement(u'content:encoded', item['content_encoded'])


class LatestMoviesFeed(Feed):
    title = "Net Feeds - las ultimas peliculas y series de Netflix"
    link = "/"
    description = "Notificaciones sobre el nuevo contenido agregado a Netflix."
    ttl = 60*12

    author_name = 'Net Feeds'
    author_link = 'http://www.net-feeds.com'
    author_mail = 'feedback@net-feeds.com'
    
    feed_type = ExtendedRSSFeed

    def items(self):
        return get_dates()

    def item_extra_kwargs(self, item):
        return {'content_encoded': self.item_content_encoded(item)}

    def item_title(self, item):
        return self._format_title(item)

    def item_pubdate(self, item):
        return item

    def item_description(self, item):
        return u'Net-feeds.com te notifica de los nuevos estrenos de Netflix.'

    def item_link(self, item):
        return '/%s/%s/%s?from=rss' % (item.year, item.month, item.day)

    def item_content_encoded(self, item):
        return render_day(item)

    def _format_title(self, day):
        mc = models.get_new_movies_count(day)
        sc = models.get_new_series_count(day)
        ec = models.get_new_episode_count(day)

        movies = QuantityExpression(u'una', u'película', u'películas')
        series = QuantityExpression(u'una', u'serie', u'series')
        episodes = QuantityExpression(u'un', u'episodio', u'episodios')

        items = []
        items.append(movies.apply(mc))
        items.append(series.apply(sc))
        items.append(episodes.apply(ec))
        
        title = HumanEnumeration()
        title = title.apply(items)
        
        return title.capitalize()


def __add_pic_url_to_infos(infos, replace):
    for i in infos:
        if hasattr(i, 'movie'):
            if replace:
                i.pic_url = i.movie.pic_url.replace('gsd', '166')
            else:
                i.pic_url = i.movie.pic_url
        elif hasattr(i, 'serie'):
            if replace:
                i.pic_url = i.serie.pic_url.replace('gsd', '166')
            else:
                i.pic_url = i.serie.pic_url

def __get_new_episodes_of_day(day):
    ne_set = set() #Using a set to avoid duplicated Series info
    new_episodes = Info.objects.filter(episode__isnull=False, timestamp=day)
    for e in new_episodes:
        info = None
        if e.episode.season:
            info = e.episode.season.serie.info

        if info:
            if e.timestamp == info.timestamp:
                continue
            info.new_episode = True
            info.timestamp = day
            ne_set.add(info)

    return ne_set

def render_day(day):
    movies = Info.objects.filter(timestamp=day).filter(movie__isnull=False).order_by('title')

    q = Q(serie__isnull=False) 
    series = Info.objects.filter(timestamp=day).filter(q).order_by('title')
    
    episodes = __get_new_episodes_of_day(day)
    
    __add_pic_url_to_infos(movies, False)
    __add_pic_url_to_infos(series, False)
    __add_pic_url_to_infos(episodes, False)

    rendered = render_to_string('feeds/day.html', {'movies': movies, 'series': series, 'episodes': episodes})
    return rendered

def get_dates():
    today = datetime.date.today()
    day0 = Info.objects.aggregate(Min('timestamp'))['timestamp__min']
    dates_list = Info.objects.exclude(timestamp=day0).dates('timestamp', 'day', order='DESC')[:10]
    return dates_list

