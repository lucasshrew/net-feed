from django.conf.urls.defaults import patterns, url

from feeds import LatestMoviesFeed

urlpatterns = patterns('feeds.views',
            url(r'^rss/$', LatestMoviesFeed()),
)
