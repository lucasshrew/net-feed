from django import template
import datetime

register = template.Library()

@register.filter
def to_string(value):
    months = ('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre')

    if value == '' or value == None:
        return ''

    if isinstance(value, int):
        return months[value-1]
    
    if datetime.date(value.year, value.month, value.day) == datetime.date.today():
        return "Hoy"

    date = "%d de %s" % (value.day, months[value.month-1])
    if value.year != datetime.date.today().year:
        date = date + ' de ' + str(value.year)

    return date
