from django.conf.urls.defaults import patterns, url

urlpatterns = patterns('homepage.views',
    url(r'^$', 'index'),
    url(r'^(?P<uid>\d+)$', 'info'),
    url(r'^(?P<year>\d+)/(?P<month>\d+)/(?P<day>\d+)$', 'day'),
    url(r'^next/$', 'next'),
    url(r'^duedates/$', 'due_date'),
)
