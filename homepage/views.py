# coding: utf-8

from django.shortcuts import render_to_response
from django.db.models import Q, Min, Max
from django.core.cache import cache
from django.views.decorators.cache import cache_page
from django.template import RequestContext, loader

import datetime
import calendar
import os 
import settings

from updatedb.models import Info
import log
from log import logger

logger.setLevel(log.WARNING)

def get_diary(info_list, today):
    def this_month(t):
        return today.year == t.year and today.month == t.month

    s = set([i.timestamp for i in info_list if this_month(i.timestamp)])
    diary = calendar.monthcalendar(today.year, today.month)
    for d in s:
        for week in diary:
            try:
                week[week.index(d.day)] = (d.day, "/%i/%i/%i" % (d.year, d.month, d.day))
            except ValueError:
                pass

    return diary


def info(request, uid):
    group_by_day, diary, today = get_index_info(0)
    return render_to_response('homepage/info.html', {'info': Info.objects.filter(uid=uid)[0], 
        'diary':diary, 
        'date':today, 
        'today':today,
        'due_dates_count': get_duedate_content()['count']
        })


def __movie_and_serie_info_q():
    return Q(movie__isnull=False)|Q(serie__isnull=False)


def __add_pic_url_to_infos(infos, replace):
    for i in infos:
        if hasattr(i, 'movie'):
            i.pic_url = i.movie.pic_url
        elif hasattr(i, 'serie'):
            i.pic_url = i.serie.pic_url


def __get_new_episodes_of_day(day):
    ne_set = set() #Using a set to avoid duplicated Series info
    new_episodes = Info.objects.filter(episode__isnull=False, timestamp=day)
    for e in new_episodes:
        info = None
        if e.episode.season:
            info = e.episode.season.serie.info

        if info:
            if e.timestamp == info.timestamp:
                continue
            info.new_episode = True
            info.timestamp = day
            ne_set.add(info)

    return ne_set

def day(request, year, month, day):
    logger.debug('requesting day page %s', str(day))
    cache_key = __generate_cache_key(datetime.date(int(year), int(month), int(day)), 0, 'day')

    logger.debug('cache key is %s', cache_key)
    if cache.get(cache_key, 'has_expired') == 'has_expired':
        logger.debug('cache expired')
        day = datetime.date(int(year), int(month), int(day))
        info_list = Info.objects.all().order_by('-timestamp')
        day0 = Info.objects.aggregate(Min('timestamp'))['timestamp__min']
        diary = get_diary(info_list.exclude(timestamp=day0), day)

        added = Info.objects.filter(timestamp=day).filter(__movie_and_serie_info_q()).order_by('title')

        ne_set = __get_new_episodes_of_day(day)
        ne_set.update(added)
        __add_pic_url_to_infos(ne_set, False)

        added = list(ne_set)
        added.sort(lambda a, b: cmp(a.title, b.title))

        cache.set(cache_key+'added', added)
        cache.set(cache_key+'date', day)
        cache.set(cache_key+'diary', diary)
        cache.set(cache_key, 'cached')

    return render_to_response('homepage/day.html', {'added': cache.get(cache_key+'added'), 
        'date':cache.get(cache_key+'date'), 
        'diary': cache.get(cache_key+'diary'), 
        'today': datetime.date.today,
        'due_dates_count': get_duedate_content()['count']
        },
        context_instance=RequestContext(request))


def get_next(day_list, step):
    group_by_day = []

    for day in day_list[step:step+4]:
        added = Info.objects.filter(timestamp=day).filter(__movie_and_serie_info_q()).order_by('title')
        new_eps = __get_new_episodes_of_day(day)
        new_eps.update(added)
        __add_pic_url_to_infos(new_eps, True)
        added = list(new_eps)
        added.sort(lambda a, b: cmp(a.title, b.title))
        group_by_day.append(added)

    return group_by_day


def get_group_by_day(step):
    day0 = Info.objects.aggregate(Min('timestamp'))['timestamp__min']
    day_list = Info.objects.exclude(timestamp=day0).dates('timestamp', 'day', order='DESC')
    return get_next(day_list, step)


def get_index_info(step):
    today = datetime.date.today()
    day0 = Info.objects.aggregate(Min('timestamp'))['timestamp__min']
    
    day_list = Info.objects.exclude(timestamp=day0).dates('timestamp', 'day', order='DESC')
    group_by_day = get_next(day_list, step)

    info_list = Info.objects.exclude(timestamp=day0)
    diary = get_diary(info_list, today)

    return group_by_day, diary, today


def __generate_cache_key(day, page, type):
    return '%s:%d:%d:%s' % (day, Info.objects.count(), page, type)


def index(request):
    diary = None
    today = datetime.date.today()
    group_by_day = None

    request.session['page'] = 0

    logger.debug('requesting index')
    logger.debug('generating cache key...')

    cache_key = __generate_cache_key(Info.objects.all().aggregate(Max('timestamp'))['timestamp__max'], 0, 'next')
    logger.debug('key is.. [%s]', cache_key)

    if cache.get(cache_key, 'has_expired') == 'has_expired':
        logger.debug('cache expired')
        group_by_day = get_group_by_day(0)
        day0 = Info.objects.aggregate(Min('timestamp'))['timestamp__min']
        info_list = Info.objects.exclude(timestamp=day0)
        diary = get_diary(info_list, today)
        cache.set('diary', diary)
        cache.set(cache_key, loader.render_to_string('homepage/next.html', {'info_list': group_by_day, 'today': today}))

    diary = cache.get('diary')
    return render_to_response('homepage/index.html', 
                              {'cache_page': cache.get(cache_key), 'diary': diary, 'today': today, 
                                'date':today,
                                'due_dates_count': get_duedate_content()['count']
                                  },
        context_instance=RequestContext(request))


def next(request):
    page = request.session['page'] + 4
    request.session['page'] = page 

    cache_key = __generate_cache_key(Info.objects.all().aggregate(Max('timestamp'))['timestamp__max'], page, 'next')

    if cache.get(cache_key, 'has_expired') == 'has_expired':
        group_by_day, diary, today = get_index_info(page)
        cache.set(cache_key, render_to_response('homepage/next.html', {'info_list': group_by_day, 'diary': diary, 
            'date': today, 
            'today': today,
            'due_dates_count': get_duedate_content()['count']
            }))

    return cache.get(cache_key)


def get_duedates_cache_key():
    day = datetime.date.today()
    return '%s%s' % (day, 'duedates')


def get_duedate_content():
    cache_key = get_duedates_cache_key()

    if cache.get(cache_key) is None:
        day = datetime.date.today()
        info_list = Info.objects.all().order_by('-timestamp')
        day0 = Info.objects.aggregate(Min('timestamp'))['timestamp__min']
        diary = get_diary(info_list.exclude(timestamp=day0), day)

        content = Info.objects.filter(due_date__gt=day).filter(__movie_and_serie_info_q()).order_by('due_date')

        content_count = content.count()
        title = 'Próximos vencimientos (%s)' % content_count
        __add_pic_url_to_infos(content, False)

        CACHE_EXPIRES = 60 * 60 * 5
        cache.set(cache_key, {'title': title, 'content': content, 'count':content_count}, CACHE_EXPIRES)
        cache.set(cache_key + 'diary', diary, CACHE_EXPIRES)

    return cache.get(cache_key)


def due_date(request):
    data = get_duedate_content()
    diary = cache.get(get_duedates_cache_key() + 'diary')
    day = datetime.date.today()

    return render_to_response('homepage/day.html', 
        {
            'title': data['title'], 
            'added': data['content'], 
            'date': day, 
            'diary': diary,
            'today': day,
            'is_due_date':True,
            'due_dates_count': data['count']
        }, context_instance=RequestContext(request))


