#! /usr/bin/python2.6
# coding: utf-8

import logging

from configs import LOGGER
from configs import URL

LOGGER.DEFAULT_LEVEL = logging.INFO
LOGGER.USE_STDOUT = False
LOGGER.FILE = 'info_updater.log'

from log import logger

import db
import browser
from soup import get_movie_info

def process_info_list(infos):
    infos = set(infos)

    logger.info('Starting to process %d ids', len(infos))

    failed_ids = set()
    updated_ids = set()

    for uid in infos:
        try:
            browser.go_to(URL.WIMOVIE + str(uid))
        except (browser.LinkNotFoundError, browser.NavigationError):
            failed_ids.add(uid)
            continue
        
        page = browser.get_page()
        data = get_movie_info(page, uid)

        if data.has_key('due_date'):
            if db.update_info_due_date(uid, data['due_date']):
                updated_ids.add(uid)

        if data.has_key('orig_title'):
            if db.update_info_orig_title(uid, data['orig_title']):
                updated_ids.add(uid)

    return updated_ids, failed_ids

def process_all_infos():
    infos = db.get_main_content_infos()
    return process_info_list(i[0] for i in infos)

def main():
    logger.info('Info due dates scrapping started')
    
    updated, failed = process_all_infos()

    logger.info('Info of content updated: %d', len(updated))
    logger.info('Info added to: %s', str(updated))

    logger.info('Info, re-trying failed ids')

    updated, failed = process_info_list(failed)
    
    logger.info('Info of content failed: %d', len(failed))
    logger.info('Failed ids: %s', str(failed))
    
    logger.info('Info of content updated: %d', len(updated))
    logger.info('Info added to: %s', str(updated))

if __name__ == '__main__':
    main()
    
