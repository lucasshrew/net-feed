# coding: utf-8
import sys, os
import time
import logging
import logging.handlers
from logging import WARNING, INFO, DEBUG

from optparse import OptionParser
try:
    import curses
except ImportError:
    curses = None
    
from configs import LOGGER as CONF
import settings

class LogFormatter(logging.Formatter):
    """
    Implements colors in logger
    """
    def __init__(self, color, *args, **kwargs):
        logging.Formatter.__init__(self, *args, **kwargs)
        
        self._color = color
        if color:
            fg_color = unicode(curses.tigetstr('setaf') or
                               curses.tigetstr('setf') or '', 'ascii')
            self._colors = {
                logging.DEBUG: unicode(curses.tparm(fg_color, 4), # Blue
                                       'ascii'),
                logging.INFO: unicode(curses.tparm(fg_color, 2), # Green
                                      'ascii'),
                logging.WARNING: unicode(curses.tparm(fg_color, 3), # Yellow
                                         'ascii'),
                logging.ERROR: unicode(curses.tparm(fg_color, 1), # Red
                                       'ascii'),
            }
            self._normal = unicode(curses.tigetstr('sgr0'), 'ascii')


    def format(self, record):
        try:
            record.message = record.getMessage()
        except UnicodeError, uex:
            record.message = 'Bad message (%r): %r' % (uex, record.__dict__)
            
        record.asctime = time.strftime('%y%m%d %H:%M:%S', self.converter(record.created))
        prefix = '[%(levelname)1.1s %(asctime)s %(module)s:%(lineno)d]:' % record.__dict__
        
        if self._color:
            pack = self._colors.get(record.levelno, self._normal), prefix, self._normal
            prefix = ('%s%s%s' % pack)
        
        formatted = '%s %s' % (prefix, record.message)
        if record.exc_info:
            if not record.exc_text:
                record.exc_text = self.formatException(record.exc_info)
        if record.exc_text:
            formatted = '%s\n%s' % (formatted.rstrip(), record.exc_text)
        return formatted.replace("\n", "\n ")        
             
                

def __get_handler():
    parser = OptionParser()
    parser.add_option('-q','--quiet', action='store_false', dest='verbose', default=True,
                      help='Send the log ouput to a file. Check \'LOGGER.FILE\' in configs.py module')
    options,args = parser.parse_args(sys.argv)
    if not CONF.USE_STDOUT or not options.verbose:
        return logging.handlers.RotatingFileHandler(filename=settings.PROJECT_PATH+os.sep+CONF.FILE, 
                                                    maxBytes=CONF.MAX_BYTES, 
                                                    backupCount=CONF.BACKUP_COUNT)
    return logging.StreamHandler()

    
def get_logger():
    """Build the logger"""
    root_logger = logging.getLogger()
    color = False
    if CONF.USE_STDOUT:
        if curses:
            try:
                curses.setupterm()
                if curses.tigetnum('colors'):
                    color = True
            except Exception:
                pass
    channel = __get_handler()
    channel.setFormatter(LogFormatter(color))
    root_logger.addHandler(channel)
    root_logger.setLevel(CONF.DEFAULT_LEVEL)
    return root_logger


logger = get_logger()    

def except_hook_logger(type, value, traceback):
    """Custom exception hook that forwards exceptions to logger"""
    exc_info = sys.exc_info
    sys.exc_info = lambda:(type, value, traceback)
    logger.exception('Unhandled exception raised')
    sys.exc_info = exc_info
    sys.exc_clear()

# We override system default exception hook to log all unhandled exceptions    
sys.excepthook = except_hook_logger

if __name__ == '__main__':
    logger.debug('debug')
    logger.info('info')
    logger.warn('warn')
    logger.error('error')
    logger.critical('critical')
    try:
        1/0
    except:
        logger.exception('exception')
    
    
    
