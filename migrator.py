import db
from updatedb.models import SeasonlessSerie, Season, Info, Serie

if __name__ == '__main__':
    print "ss count", SeasonlessSerie.objects.count()
    for ss in SeasonlessSerie.objects.all():
        print ss.info.title, ss.episode_set.count()

        try:
            serie0 = Serie(info=ss.info, pic_url=ss.pic_url)
            serie0.save()

            s = Season(info=ss.info, serie=serie0)
        
            for e in ss.episode_set.all():
                s.episode_set.add(e)

            ss.episode_set.clear()

            s.save()
        except:
            pass
    SeasonlessSerie.objects.all().delete()
    print 'end'
