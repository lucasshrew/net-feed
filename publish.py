
import logging
from configs import LOGGER

LOGGER.DEFAULT_LEVEL = logging.DEBUG
LOGGER.USE_STDOUT = False
LOGGER.FILE = 'publisher.log'

from log import logger
from social import twitter

if __name__ == '__main__':
    logger.info('Publishing updates to social networks')
    twitter.Twit()()
    logger.info('Info published OK.')

