BeautifulSoup==3.2.0
Django==1.5.1
Fabric==1.3.4
MySQL-python==1.2.3
distribute==0.6.24
httplib2==0.7.4
ipython==0.12
mechanize==0.2.5
oauth2==1.5.211
pycrypto==2.4.1
pytz==2011n
requests==0.14.2
six==1.1.0
ssh==1.7.11
wsgiref==0.1.2
zope.event==3.5.1
zope.interface==3.8.0
zope.schema==4.0.1
zope.testbrowser==4.0.2
