import db
from updatedb.models import Info, Movie
import time
import threading
from urllib2 import urlopen
import json
import difflib

import logging

from configs import LOGGER
from configs import URL

LOGGER.DEFAULT_LEVEL = logging.INFO
LOGGER.USE_STDOUT = False
LOGGER.FILE = 'rotten.log'

from log import logger 

#OLD_KEY = "gdmns9pb48tvmhd9q7y3hsh8"
KEY = "9v3n63tujah4ed7rgcq5sjxg"
URL = "http://api.rottentomatoes.com/api/public/v1.0.json?apikey="+KEY

log = open("orig_title.log", "a")


def get_json(url):
    response = urlopen(url.encode('utf8'))
    o = response.read()

    if response.getcode() == 200:
        return json.loads(o)

def get_closest_match(jobj, title):
    possibilites = []
    dir = {}
    for info in jobj['movies']:
        possibilites.append(info['title'])
        dir[info['title']] = info

    if len(possibilites) == 0:
        return jobj

    match = difflib.get_close_matches(title, possibilites, 1)
    
    return dir[match[0]] if len(match)>0 else {}


def get_json_of_movie(movie, movie_url):
    movie_url = movie_url+u'?apikey='+KEY+u'&q='+movie.info.title.replace(' ', '+')+u'&page_limit=10'
    return get_closest_match(get_json(movie_url), movie.info.title)


def save_score(movie, json):
    logger.info(u"saving score of %s. json is %s", movie.info.title, json)
    try:
        score = int(json['ratings']['critics_score'])
        if score == -1:
            score = int(json['ratings']['audience_score'])
    except (KeyError, IndexError):
        logger.debug(u"IndexError: movie: "+movie.info.title+" was not found on rotten tomatoes! ("+movie.info.orig_title+u").\n")
        return

    movie.info.rotten_score = score
    print u"score of", movie.info.title, "is", movie.info.rotten_score
    movie.info.save()


def save_url(movie, json):
    logger.info(u"saving url of %s. json is %s", movie.info.title, json)
    try:
        url = json['links']['alternate']
    except (KeyError, IndexError):
        logger.debug(u"IndexError: movie: "+movie.info.title+u" was not found on rotten tomatoes! ("+movie.info.orig_title+u").\n")
        return

    movie.info.rotten_url = url
    print "url of", movie.info.title, "is", movie.info.rotten_url
    movie.info.save()


def fetch_score_with_title(movie, movie_url):
    json = get_json_of_movie(movie, movie_url)

    save_score(movie, json)
    save_url(movie, json)


def fetch_score(movie, movie_url):
    if movie.info.orig_title == "":
        return fetch_score_with_title(movie, movie_url)

    json = get_json_of_movie(movie, movie_url)

    try:
        save_score(movie, json)
        save_url(movie, json)
    except IndexError:
        time.sleep(0.3)
        log.write(movie.info.orig_title + u" failed to load. trying with title\n")
        fetch_score_with_title(movie, movie_url)


def create_thread(movie, movie_url):
    t = threading.Thread(target=fetch_score, args=(movie, movie_url))
    t.start()
    return t


def get_movie_url():
    return get_json(URL)['links']['movies']


def main():
    threads = []
    movie_url = get_movie_url()
    range = 0

    while True:
        for movie in Movie.objects.filter(info__rotten_score__isnull=True)[range:range+10]:
            threads.append(create_thread(movie, movie_url))
        
        if len(threads) == 0:
            break

        for t in threads:
            t.join()

        threads = []
        range += 10
        print range
        time.sleep(2)


if __name__ == '__main__':
    main()
