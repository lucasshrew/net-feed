#! /bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR

# virtual env settings
export WORKON_HOME=/home/netfeed/.virtualenvs
source /usr/local/bin/virtualenvwrapper.sh

# set current virtual env
workon netfeeds

# set project env variables
source ./env-setup.sh

# run content information updater bot
python info_updater.py
