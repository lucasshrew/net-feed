# Load the defaults in order to then add/override with production-only settings
# by default we load dev settings, since in production we load the settings explicitly in our WSGI config script.
from dev import *
