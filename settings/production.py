# Load defautls in order to then add/override with production-only settings
from defaults import *

# We override DEBUG setting disabling it
DEBUG = False
TEMPLATE_DEBUG = False

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = [".net-feeds.com"]

SECRET_KEY = "ykvy0z1ujceh@499xjcy4mm-fy!oxi6ypariqs+7+2ewej%^i^"
