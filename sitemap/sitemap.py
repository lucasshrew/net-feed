
import datetime
from collections import namedtuple


from django.contrib.sitemaps import Sitemap
from django.db.models import Q, Min, Max

from updatedb.models import Info


SitemapEntry = namedtuple('SitemapEntry', ['name', 'location', 'lastmod', 'frequency'])

class MainSitemap(Sitemap):
    def items(self):
        today = datetime.date.today()

        index = SitemapEntry('index', '/', get_max_day(), 'daily')
        due_dates = SitemapEntry('Vencimientos', '/duedates/', today, 'daily')

        content = index, due_dates
        return content

    def lastmod(self, obj):
        return obj.lastmod

    def location(self, obj):
        return obj.location

    def changefreq(self, obj):
        return obj.frequency

#TODO: refactor later to models and rename
def get_max_day():
    return Info.objects.aggregate(Max('timestamp'))['timestamp__max']
