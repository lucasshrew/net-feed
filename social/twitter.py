# -*- coding: utf-8 -*-

import db
import oauth2 as oauth
from urlparse import parse_qs
from urllib import urlencode
from updatedb.models import Movie, Serie, get_new_episode_count
import datetime, socket, random
from settings import dev

from log import logger

def request_verify_code():
    print "START"
    consumer = oauth.Consumer('aqiq87sICT8wWQDh0Uwdg', 'fpg2WGLdH4V9s8YsEJRD4psNIgRVqHuPu2YRBHIpSU')
    client = oauth.Client(consumer)

    print "REQUESTING TOKEN"
    response, content = client.request('https://api.twitter.com/oauth/request_token')
    print response, content
    print 30*'-'
    res = parse_qs(content)
    print res
    print res['oauth_token'][0], res['oauth_token_secret'][0]

    print "AUTHORIZING"
    print 'Click in link to get an authorization code'
    print 'https://api.twitter.com/oauth/authorize?oauth_token='+res['oauth_token'][0]
    print 30*'-'

#paste link code
    link_code = raw_input('link code: ')

    token = oauth.Token(res['oauth_token'][0], res['oauth_token_secret'][0])
    token.set_verifier(link_code)
    client = oauth.Client(consumer, token)

    print "ACCESING TOKEN"
    response, content = client.request('https://api.twitter.com/oauth/access_token')
    print response, content
    print 30*'-'
    res = parse_qs(content)
    print res
    print 'new tokens. should save them for later'
    print res['oauth_token'][0], res['oauth_token_secret'][0]

    print "twitting"
#twit
    Twit(res['oauth_token'][0], res['oauth_token_secret'][0])()

#print response, content


class Twit:
    def __call__(self):
        logger.debug('gethostname [%s]', socket.gethostname())
        if socket.gethostname() != 'net-feeds.com':
            return

        logger.debug('is on server. twitting...')
        consumer = oauth.Consumer('aqiq87sICT8wWQDh0Uwdg', 'fpg2WGLdH4V9s8YsEJRD4psNIgRVqHuPu2YRBHIpSU')
        token = oauth.Token('474393508-DomTMpekvoKjRDz41bmXDu2p9w28PjrghhhEQdSm', 'N5vmAWCuQzAXIXqU4eKZGkMaWOq7P6Z3KkeGxLFdDQ')
        client = oauth.Client(consumer, token)
        today = datetime.date.today()
        movie_count = Movie.objects.filter(info__timestamp=today).count()
        serie_count = Serie.objects.filter(info__timestamp=today).count()
        episode_count = get_new_episode_count(today)

        post_data = {}

        logger.debug('movie and serie and episode %d, %d, %d' % (movie_count, serie_count, episode_count))
        if movie_count == 0 and serie_count == 0 and episode_count == 0:
            logger.debug('movie and serie and episode is zarro')
            return

        film = "películas"
        film_full = "películas nuevas"
        serie = "series"
        serie_full = "series nuevas"
        episode = "episodios"
        episode_full = "episodios nuevos"

        if movie_count == 1:
            film = "película"
            film_full = film+' nueva'

        if serie_count == 1:
            serie = "serie"
            serie_full = serie+' nueva'

        if episode_count == 1:
            episode = "episodio"
            episode_full = episode+' nuevo'

        s = random.choice(['Nuevo contenido:', 'Nuevo en netflix:', 'Lo mas reciente:', 'Lo último:', 'Catalogo actualizado:', 'Genial: '])

        if movie_count > 0:
            if serie_count > 0 or episode_count > 0:
                s += ' %d %s,' % (movie_count, film)
            else:
                s += ' %d %s' % (movie_count, film_full)
                
        if serie_count > 0:
            if episode_count > 0:
                s += ' %d %s,' % (serie_count, serie)
            else:
                s += ' %d %s' % (serie_count, serie_full)

        if episode_count > 0:
            s += ' %d %s' % (episode_count, episode_full)

        part = s.rpartition(',')
        if part[0] != '':
            s = '%s y%s' % (part[0], part[2])

        s += ' para disfrutar. http://www.net-feeds.com/%d/%d/%d' % (today.year, today.month, today.day)

        post_data['status'] = s
        response, content = client.request(
                    'https://api.twitter.com/1.1/statuses/update.json',
                    method='POST',
                    body=urlencode(post_data)
                )
        logger.debug('response: %s', response)
        logger.debug('content: %s', content)


if __name__ == '__main__':
    Twit()()
