# coding: utf-8

import os
import re
import gc
import traceback
import datetime
import logging

from BeautifulSoup import BeautifulSoup

from urlparse import parse_qs
from urlparse import urlsplit
from urllib import urlencode

from os.path import join as pathjoin
from json import loads


import db
import browser

from configs import URL
from configs import LOGGER

from log import logger


class ParseProblem(Exception):
    def __init__(self, msg):
        super(Exception, self).__init__()
        self.msg = msg

    def __str__(self):
        return 'ParseProblem: %s' % str(self.msg)


def get_param(url, name):
    try:
        return parse_qs(url.split('?')[1])[name][0]
    except KeyError:
        return None

def get_genre_links(dirname):
    ref_set = set()

    for dirpath, dirnames, filenames in os.walk(dirname):
        for f in filenames:
            if not f.endswith('html') or f.startswith('history') or f.startswith('voted'):
                continue
            
            logger.debug('file to process: %s', f)
            
            fin = open(pathjoin(dirpath, f))
            fdata = fin.read()
            fin.close()

            soup = BeautifulSoup(fdata)
            refs = soup.findAll('a', href=re.compile('pn='))
            
            if len(refs) <= 0:
                continue
            
            if 'agid' in refs[0]['href']:
                agid = get_param(refs[0]['href'], 'agid')
            else:
                agid = None
            
            base_url = refs[0]['href'].split('?')[0]

            maxpn = 0
            for ref in refs:
                if get_param(ref['href'], 'pn') == u'1' or not 'vt=tg' in ref['href']:
                    continue

                maxpn = max(int(get_param(ref['href'], 'pn')), maxpn)
            
            params = {}
            if agid:
                params['agid'] = agid

            for pn in range(2, maxpn+1):
                params['pn'] = pn
                ref_set.add(base_url+'?'+urlencode(params))

    return ref_set


def get_pagination_links(page):
    ref_set = set()

    soup = BeautifulSoup(page)
    refs = soup.findAll('a', href=re.compile('pn='))

    base_url = refs[0]['href'].split('?')[0]

    maxpn = 0
    for ref in refs:
        maxpn = max(int(get_param(ref['href'], 'pn')), maxpn)
    
    params = {}
    for pn in range(1, maxpn+1):
        params['pn'] = pn
        ref_set.add(base_url+'?'+urlencode(params))

    return ref_set


def get_ids(dirname):
    """Return a list of ids and a dictionary of pic_urls indexed by id"""
    logger.info('Get all ids from files')

    uids = set()
    pic_urls_by_id = {}

    for dirpath, dirnames, filenames in os.walk(dirname):
        for f in filenames:
            logger.debug('Processing file: ')
            
            if f.startswith('history') or f.startswith('voted'):
                uids.update(get_ids_from_history(pathjoin(dirpath, f)))
            else:
                logger.debug('\t\t %s', f)

                fin = open(pathjoin(dirpath, f))
                fdata = fin.read()
                fin.close()

                soup = BeautifulSoup(fdata)
                refs = soup.findAll('span', {'class':re.compile('boxShot ')})
                
                for ref in refs:
                    link = ref.find('a')
                    movieid = get_param(link['href'], 'movieid')
                    if movieid:
                        id = int(movieid)
                        try:
                            img = ref.find('img')
                            pic_urls_by_id[id] = img['hsrc'] if 'hsrc' in img._getAttrMap() else img['src']
                        except Exception, e:
                            logger.error(ref.contents[0])
                            logger.error('Failed to get PIC URL from: %s, %s, %s', f, movieid, traceback.format_exc())
                        else:
                            uids.add(id)

    return uids, pic_urls_by_id


def get_ids_from_history(filepath):
    ids = set()
    fin = open(filepath)
    fdata = fin.read()
    fin.close()
    soup = BeautifulSoup(fdata)
    refs = soup.findAll('span', {'class': 'title'})

    for ref in refs:
        ref = ref('a')
        url = ref[0]['href']
        path = urlsplit(url).path
        movieid = os.path.split(path)[1]
        movieid = get_real_id(movieid)
        ids.add(int(movieid))
    return ids


def get_real_id(uid):
    browser.go_to(URL.WIMOVIE + str(uid))
    page = browser.get_page()

    soup = BeautifulSoup(page)
    pic_query = {'class': re.compile('boxShotImg'), 'src': re.compile(".*\d+\.jpg")}
    res = soup.find('img', pic_query)
    pic_url = res['src']
    pic_name = os.path.split(pic_url)[1]
    new_uid = os.path.splitext(pic_name)[0]

    if new_uid != uid:
        logger.debug('Correct ID found: %s'% new_uid)
    return new_uid


def get_metadata(mid):
    data = {}
    try:
        browser.go_to(URL.METADATA % str(mid), need_login=False)
        data = browser.get_json()
    except:
        pass
    return data


def get_movie_info(contents, mid):
    #FIXME: change name, is used for movies and series
    data = {}
    soup = BeautifulSoup(contents)

    synopsis = soup.find(['p','div'], {'class': re.compile('synopsis')})
    if synopsis and len(synopsis.contents)>0:
        data['synopsis'] = get_plain_string(synopsis)
    else:
        synopsis_parent = soup.find('div', {'class': 'synopsisData'})
        if synopsis_parent:
            synopsis = synopsis_parent.find('div', {'class': 'tagline'})
            data['synopsis'] = synopsis.string

    title = soup.find('h2', {'class': re.compile('^title$')})
    if not title:
        title = soup.find('h1', {'class': re.compile('^title$')})
    if not title:
        title = soup.find('h1', {'class': re.compile('showTitle')})

    orig_title = soup.find('span', {'class': re.compile('^origTitle$')})
    if orig_title:
        orig_title = orig_title.string[1:-1]
    else:
        orig_title = ""
    
    logger.debug("Found orig title [%s]", orig_title)
    data['orig_title'] = orig_title

    if title and title.contents:
        data['title'] = get_plain_string(title)
    else:
        data['title'] = orig_title

    sections = soup.find('div', {'class': 'clearfix'})
    if sections:
        dls = sections.findAll('dl')
        for dl in dls:
            if dl.dt and dl.dt.string == 'Disponibilidad':
                date_str = dl.dd.string
                date_str = date_str.split()

                for d in date_str:
                    try:
                        date = datetime.datetime.strptime(d, '%d/%m/%y')
                        data['due_date'] = date
                    except:
                        pass

    data['uid'] = mid
    return data


def check_info_data(data, picurl):
    try:
        if data['synopsis'] == '':
            raise ParseProblem('synopsis')
        if data['title'] == '':
            raise ParseProblem('title')
        if picurl == '':
            raise ParseProblem('picurl')
    except KeyError, e:
        raise ParseProblem(e)


def available(contents):
    soup = BeautifulSoup(contents)
    not_allowed = soup.find('div', {'class': re.compile('movie-not-allowed')})
    not_available = soup.find('a', {'class': re.compile('unplayable')})
    unplayable = soup.find('span', {'class': re.compile('unplayableText')})
    return not_allowed == None and not_available == None  and unplayable == None


def is_movie(contents):
    soup = BeautifulSoup(contents)
    episodes = soup.find('ul', {'class': re.compile('episodeList')})
    episode_content = soup.find('div', {'class': re.compile('episodesContent')})
    return bool(not episodes) and bool(not episode_content)


def dump_database(dirname):
    movie_ids = set()
    movie_ids.update(db.get_movies())
    
    series_ids = set()
    series_ids.update(db.get_series())

    episodes_ids = set()
    episodes_ids.update(db.get_series_episodes())

    list_of_ids, pic_dict = get_ids(dirname)
    all_ids = set(list_of_ids)
    
    new_ids = all_ids - movie_ids - series_ids - episodes_ids
    logger.info("new ids are: %d %s", len(new_ids), str(new_ids))

    del movie_ids
    del episodes_ids
    del all_ids

    #failed_ids = {}
    
    gc.collect()

    parse_new_ids(new_ids, pic_dict)
    #failed_ids = parse_new_ids(new_ids, pic_dict)
    #if failed_ids:
    #    logger.info('re-trying failed ids')
    #    failed_ids = parse_new_ids(failed_ids.keys(), pic_dict)

    #dump_errors(failed_ids)

    update_series_ids(series_ids, pic_dict)
    #failed_ids = update_series_ids(series_ids, pic_dict)
    #if failed_ids:
    #    logger.info('re-trying failed ids')
    #    failed_ids = update_series_ids(failed_ids.keys(), pic_dict)
    
    #dump_errors(failed_ids)
    del pic_dict
    return new_ids
   

def parse_new_ids(new_ids, pic_dict):
    failed_ids = {}
    for uid in new_ids:
        try:
            browser.go_to(URL.WIMOVIE + str(uid))
            page = browser.get_page()
            
            if not available(page):
                logger.info('Content not available yet: %s', uid)
                continue

            if is_movie(page):
                logger.debug('Is movie %d', uid)
                data = get_movie_info(page, uid)
                pic_url = pic_dict[uid]
                
                # We check for movie data consistency
                check_info_data(data, pic_url)

                # Create a Movie
                db.create_movie(data, pic_url)
            else:
                # Process uid as a serie
                update_serie(uid, page, pic_dict)
        except Exception, e:
            #failed_ids[uid] = (traceback.format_exc(), browser.get_page())
            logger.exception('parse_new_ids()')
        db.clear_cache()
        gc.collect()
    return failed_ids


def update_series_ids(series_ids, pic_dict):
    failed_ids = {}
    for uid in series_ids:
        try:
            browser.go_to(URL.WIMOVIE + str(uid))
            page = browser.get_page()

            if not available(page):
                logger.warning('Content no more available: %s', uid)
                continue

            update_serie(uid, page, pic_dict)
        except Exception, e:
            #msg = traceback.format_exc()
            #try:
            #    page = browser.get_page()
            #except:
            #    page = "Not available"
            #failed_ids[uid] = (msg, page)
            logger.exception('update_series_ids()')
        db.clear_cache()
        gc.collect()
    return failed_ids


def dump_errors(errors):
    ## dump all failed ids to a file
    error_file = open(LOGGER.ERROR_FILE, 'a')
    for uid, (msg, page) in errors.iteritems():
        error_file.write('Fail in: %s caused by: %s\n'% (uid, msg))
        page_file_path = os.path.join(LOGGER.ERROR_DIR, str(uid)+'.html')
        page_file = open(page_file_path, 'w')
        page_file.write(page)
        page_file.close()
    error_file.close()
            

def update_serie(sid, page, pic_dict):
    update_or_create_serie(sid, page, pic_dict)


def get_content_info(page, uid):
    data = get_movie_info(page, uid)

    if data.has_key('title') and data.has_key('synopsis'):
        return data

    meta = get_metadata(uid)
    if meta:
        data['title'] = meta['copy']['Title'][0]

        synopsis_key = '1000Synopsis' 
        if meta['copy'].has_key('ShortSynopsis'):
            synopsis_key = 'ShortSynopsis'
        data['synopsis'] = meta['copy'][synopsis_key][0]

    return data


def get_serie_data(uid):
    data = {}
    try:
        browser.go_to(URL.SERIE_INFO % str(uid), need_login=False)
        data = browser.get_json()
    except:
        pass
    return data


def update_or_create_serie(sid, page, pic_dict):
    serie_pk = db.get_serie_pk(sid)
    
    if not serie_pk:
        data = get_content_info(page, sid)
        pic_url = pic_dict[sid]

        # We check for movie data consistency
        check_info_data(data, pic_url)

        serie_pk = db.create_serie(data, pic_url)
    
    data = get_serie_data(sid)

    for season in data.get('episodes', []):
        for episode in season:
            season_info = {}
            season_info['uid'] = episode['seasonId']
            season_info['title'] = episode['season']
            season_info['synopsis'] = episode['season']

            season_pk = db.add_season_to_serie(season_info, serie_pk)

            info = {}
            info['uid'] = episode['episodeId']
            info['title'] = episode['title']
            info['synopsis'] = episode['synopsis']
            number = episode['episode']
            
            db.add_episode_to_season(number, info, season_pk)
    

def get_plain_string(tag):
    '''
        Returns an html tag text with inner tags stripped.
    '''
    return ''.join(x.string for x in tag.contents if x.string != None).strip()


if __name__ == '__main__':
    from configs import URL
    from configs import LOGGER

    LOGGER.DEFAULT_LEVEL = logging.DEBUG

    from log import get_logger
    get_logger()

    ref_set = get_genre_links('out')
    logger.debug("unique links: %s", len(ref_set))
    for r in ref_set:
        logger.debug('\t%s', r)

    names = get_ids('out')
    logger.debug("Films: %d", len(names))
    for n in names:
        logger.debug('\t%s', n)

