function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function changeCalendar(dir, year, month) {
    if (dir == 'prev') {
        month--;
        if (month == 0) {
            year--;
            month = 12;
        }
    } else if (dir == 'next') {
        month++;
        if (month == 13) {
            month = 1;
            year++;
        }
    }

	sendCalData(year, month);
}

function sendCalData(year, month) {
	$('#calendar_cont').css('background', 'url("/static/loading.gif") center right no-repeat');
	$('table#calendar').remove();

    $.get('/diary/'+year+'/'+month, function(data) {
        $('#calendar_cont').append(data).css('background', '');
    });
}

//twitter code
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");

var moreData = false;
var wishlist;

function listAddAction(e) {
    e.stopPropagation();

	var label = "homepage";
	var wl = $('div#wishlist');
	if (wl.length > 0) {
		label = 'wishlistpage';
	}

	_gaq.push(['_trackEvent', 'wishlist', 'add', label]);

    $(this).removeClass("list-add").addClass("hide");
    this.setAttribute('title', "Remover de la lista de deseos");
    var btn = this;
    $.post('/wishlist/add/'+this.getAttribute("uid")+'/', {}, function(data) {
        $(btn).unbind('click').click(listRemoveAction).addClass('list-remove').removeClass('hide');
    });
}

function listRemoveAction(e) {
	e.stopPropagation();

	var wl = $('div#wishlist');
	if (wl.length > 0) {
		$(this).parent().parent().next().css('display', 'none');
		$(this).parent().parent().parent().fadeOut('slow');
		$.post('/wishlist/remove/'+this.getAttribute("uid")+'/', {}, function(data) {
			console.log("remove wishlist post returned", data);
		});

		_gaq.push(['_trackEvent', 'wishlist', 'remove', 'wishlistpage']);
		return;
	}

	_gaq.push(['_trackEvent', 'wishlist', 'remove', 'homepage']);
	$(this).addClass("hide").removeClass("list-remove");
	this.setAttribute('title', "Agregar a la lista de deseos");
    var btn = this;
	$.post('/wishlist/remove/'+this.getAttribute("uid")+'/', {}, function(data) {
		console.log("remove post returned", data);
        $(btn).unbind('click').click(listAddAction).addClass('list-add').removeClass('hide');
	});
}

function removeFromWishList(uid) {
        $('div[uid="' + uid + '"]').addClass('hide');
        $.post('/wishlist/remove/'+uid+'/', {}, function(data) {
            console.log("remove wishlist post returned", data);
        });

        _gaq.push(['_trackEvent', 'wishlist', 'remove', 'wishlistapp']);

        setTimeout(function() {
            $('div[uid="'+uid+'"]').remove();
        }, 400);
}

function addClassToWishlistBtn(wishlist) {
	$('.list-add').removeClass('hide');
	for (var i=0; i<wishlist.length; i++) {
		var btn = $('.list-add[uid="'+wishlist[i]+'"]')[0];
		if (btn) {
			$(btn).removeClass("list-add").addClass("list-remove");
			btn.setAttribute('title', "Remover de la lista de deseos");
			$(btn).unbind('click').click(listRemoveAction);
		}
	}
}

function showSignIn(e) {
	e.stopPropagation();
	console.log('show', $(this).offset(), $(this).position());
	console.log('scroll', $(document).scrollTop(), $(document).scrollLeft());

	var x = $(this).offset().left - $(document).scrollLeft() + $(this).width() + 4;
	var y = $(this).offset().top - $(document).scrollTop();

	$('#add-tooltip').addClass('show').css({'position': 'fixed', 'left':x+'px', 'top':y+'px'});
}

function setWishlistButtons() {
	if (wishlist == undefined) {
		$.get('/wishlist/get/', function(data) {
			console.log("wishlist get returned");
			if (data == 'anonymous') {
				$('.list-add').removeClass('hide').unbind("click").click(showSignIn);
				return;
			}
			wishlist = JSON.parse(data);
			addClassToWishlistBtn(wishlist);
			$('.list-add').unbind("click").click(listAddAction);
		});
	} else {
		addClassToWishlistBtn(wishlist);
		$('.list-add').unbind("click").click(listAddAction);
	}
}

function fetchMoreData(e) {
    if (moreData)
        return;

    var w = e.currentTarget;
	var scroll = w.scrollY+w.innerHeight;

    if (document.body.scrollHeight - scroll < 250) {
        moreData = true;
        $.get('/next/', function(data) {
            $('.day-update').last().after(data);
            $('.wait').remove();
            if (data.trim() == "") {
                return;
            }

            moreData = false;

			setWishlistButtons();
        });
        $('.day-update').last().after('<div class="wait"></div>');
    }
}

document.addEventListener('DOMContentLoaded', function(ev) {
	var i = ev.target.URL.indexOf('/', 8);
	if (ev.target.URL.substr(i) == '/') {
		window.addEventListener('scroll', fetchMoreData);
	}
});

$(window).unload(function(ev) {
	var i = ev.target.URL.indexOf('/', 8);
	if (ev.target.URL.substr(i) == '/') {
		window.removeEventListener('scroll', fetchMoreData);
	}
});

$(window).load(function(e) {
	$.ajaxSetup({
		crossDomain: false, // obviates need for sameOrigin test
		beforeSend: function(xhr, settings) {
			if (!csrfSafeMethod(settings.type)) {
				xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
			}
		}
	});

    if (document.location.href.indexOf('#') > 0) {
		$(document).scrollTop($(document).scrollTop()-70);
	}

	setWishlistButtons();
    $('nav').unbind('click').click(function(e) {
        e.stopPropagation();
        $(this).toggleClass('show');
    });

    $('body').unbind('click').click(function(e) {
        $('nav').removeClass('show');
		$('#add-tooltip').removeClass('show');
    });

	$('#navbar>ul>a[href="'+window.location.pathname+'"]>li').addClass('selected');
});
