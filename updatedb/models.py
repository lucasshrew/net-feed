from django.db import models

class Info(models.Model):
    uid = models.IntegerField(db_index=True)
    genre = models.CharField(max_length=200, null=True)
    timestamp = models.DateField(auto_now_add=True)
    synopsis = models.TextField()
    title = models.CharField(max_length=200)
    due_date = models.DateField(auto_now_add=False, null=True)
    orig_title = models.CharField(max_length=200)
    rotten_score = models.IntegerField(null=True)
    rotten_url = models.CharField(max_length=200)

    def __unicode__(self):
        return str(self.uid) + ': ' + str(self.title)

    def pic_url(self):
        if hasattr(self, 'movie'):
            return self.movie.pic_url
        if hasattr(self, 'serie'):
            return self.serie.pic_url


class Serie(models.Model):
    info = models.OneToOneField(Info)
    pic_url = models.CharField(max_length=100)

    def __unicode__(self):
        return self.info.__unicode__()


class Season(models.Model):
    info = models.OneToOneField(Info)
    serie = models.ForeignKey(Serie)


class Episode(models.Model):
    info = models.OneToOneField(Info)
    number = models.IntegerField()
    season = models.ForeignKey(Season, null=True)


class Movie(models.Model):
    info = models.OneToOneField(Info)
    pic_url = models.CharField(max_length=100)

    def get_absolute_url(self):
        return "/%d/"% self.info.id

    def __unicode__(self):
        return self.info.__unicode__()


def get_new_episode_count(aday):
    return Info.objects.filter(timestamp=aday).filter(episode__isnull=False).exclude(movie__isnull=False).exclude(episode__season__serie__info__timestamp=aday).count()

def get_new_movies_count(aday):
    return Movie.objects.filter(info__timestamp=aday).count()

def get_new_series_count(aday):
    return Serie.objects.filter(info__timestamp=aday).count()

