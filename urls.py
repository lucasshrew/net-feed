from django.conf.urls.defaults import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from sitemap.sitemap import MainSitemap

sitemaps = {'home': MainSitemap}

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'net_feeds.views.home', name='home'),
    # url(r'^net_feeds/', include('net_feeds.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
    url(r'^', include('homepage.urls')),
    url(r'^', include('feeds.urls')),
    url(r'^errors/$', include('errors.urls')),
    url(r'^feedback/', include('feedback.urls')),
    url(r'^accounts/', include('accounts.urls')),
    url(r'^diary/', include('diary.urls')),
    url(r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps}),
    url(r'^wishlist/', include('wishlist.urls')),
)

urlpatterns += staticfiles_urlpatterns()
