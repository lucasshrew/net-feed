
import itertools

class QuantityExpression(object):
    def __init__(self, countexpr, singular, plural):
        self.countexpression = countexpr
        self.singular = singular
        self.plural = plural

    def apply(self, count):
        head = None
        tail = None

        if count == 1:
            head = self.countexpression
            tail = self.singular
        elif count > 1:
            head = u'%d' % count
            tail = self.plural
        else:
            return ''
        return u' '.join((head, tail))

class HumanEnumeration(object):
    def __init__(self, intersep=u', ', lastsep=u' y '):
        self.inter_sep = intersep
        self.last_sep = lastsep

    def apply(self, items):
        items = list(itertools.ifilter(lambda x: len(x)>0, items)) 
        # We took the first term
        result = items[0]

        # We cycle through all internal terms
        for i in items[1:-1]:
            result = result + self.inter_sep + i

        # We add last term
        if len(items) > 1:
            result = result + self.last_sep + items[-1]
        return result

