"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from django.test.client import Client
from django.contrib.auth.models import User
from accounts.models import UserProfile
from updatedb.models import Info
import random
import json

class SimpleTest(TestCase):
    def setUp(self):
        user = User.objects.create_user('test', 'email@example.com', 'pass')
        user.is_active = True
        user.save()
        self._user = user

    def tearDown(self):
        p = self._user.get_profile()
        p.wish_list.clear()
        self._user.save()

    def _create_info(self, uid=random.getrandbits(16), title='test'):
        info = Info(uid=int(uid), title=title+str(uid))
        info.save()
        return info

    def test_get(self):
        c = Client()
        response = c.get('/wishlist/get/')
        self.assertEqual(response.status_code, 405)

        user = self._user
        p = user.get_profile()

        uids = []

        info = self._create_info()
        p.wish_list.add(info)
        uids.append(info.uid)

        info = self._create_info()
        p.wish_list.add(info)
        uids.append(info.uid)
        
        user.save()

        c.login(username="test", password="pass")
        response = c.get('/wishlist/get/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.dumps(uids), response.content)

    def test_get_view(self):
        c = Client()
        p = self._user.get_profile()

        info = self._create_info()
        p.wish_list.add(info)

        info = self._create_info()
        p.wish_list.add(info)

        info = (self._create_info(), self._create_info())
        c.login(username='test', password='pass')

        response = c.get('/wishlist/view/')

        self.assertEqual(response.status_code, 200)

        self.assertEqual(2, len(response.context['added']))

    def test_add(self):
        c = Client()
        p = self._user.get_profile()

        info = self._create_info()
        
        response = c.get('/wishlist/add/%d/'% info.uid)
        self.assertEqual(response.status_code, 405)
        
        c.login(username="test", password="pass")

        response = c.get('/wishlist/add/%d/'% info.uid)
        self.assertEqual(response.status_code, 200)
        
        i = p.wish_list.get(uid=info.uid)
        self.assertEqual(info, i)

        response = c.get('/wishlist/add/%d/'% info.uid)
        self.assertEqual(response.status_code, 200)
        
        infos = p.wish_list.filter(uid=info.uid)
        self.assertEqual(len(infos), 1)

    def test_remove(self):
        c = Client()
        p = self._user.get_profile()

        info = self._create_info()
        p.wish_list.add(info)
        self._user.save()
        
        response = c.get('/wishlist/remove/%d/'% info.uid)
        self.assertEqual(response.status_code, 405)
        
        c.login(username="test", password="pass")
        
        response = c.get('/wishlist/remove/%d/'% info.uid)
        self.assertEqual(response.status_code, 200)
        
        self.assertRaises(Info.DoesNotExist, p.wish_list.get, uid=info.uid)
        
        i = Info.objects.filter(uid=info.uid)
        self.assertFalse(len(i)==0)

