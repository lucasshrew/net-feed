from django.conf.urls.defaults import patterns, url

from accounts.forms import CustomPwdResetForm

urlpatterns = patterns('wishlist.views',
    url(r'^add/(?P<uid>\d+)/$', 'add'),
    url(r'^remove/(?P<uid>\d+)/$', 'remove'),
    url(r'^get/$', 'get'),
    url(r'^view/$', 'view'),
    url(r'^app/$', 'app')
)

