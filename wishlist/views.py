# Create your views here.
import json
import datetime

from django.http import HttpResponseNotAllowed, HttpResponse, HttpResponseNotFound, HttpResponseRedirect
from django.shortcuts import render
from django.db.models import Min
from django.contrib.auth.decorators import login_required
from django.core.cache import cache
from homepage.views import get_duedate_content

from updatedb.models import Info
from homepage.views import get_diary


def gen_cache_key(request):
    return 'user' + str(request.user.pk)

def add(request, uid):
    if request.user.is_anonymous():
        return HttpResponseNotAllowed("user is anonymous") # TODO: change to redirect to login page with next statement

    try:
        info = Info.objects.get(uid=uid)
        p = request.user.get_profile()

        p.wish_list.add(info)
        p.save()

        cache_key = gen_cache_key(request)
        cache.set(cache_key, "expired")
    except Info.DoesNotExist:
        return HttpResponseNotFound("wrong uid")

    return HttpResponse("OK")

def remove(request, uid):
    if request.user.is_anonymous():
        return HttpResponseNotAllowed("user is anonymous") # TODO: change to redirect to login page with next statement

    try:
        p = request.user.get_profile()
        info = p.wish_list.get(uid=uid)
        p.wish_list.remove(info)
        cache_key = gen_cache_key(request)
        cache.set(cache_key, "expired")
    except Info.DoesNotExist:
        return HttpResponseNotFound("uid not in wishlist")

    return HttpResponse('OK')


def app(request):
    if request.user.is_anonymous():
        return HttpResponseRedirect("/accounts/login/app/") # TODO: change to redirect to login page with next statement)

    day = datetime.date.today()
    info_list = Info.objects.all().order_by('-timestamp')
    day0 = Info.objects.aggregate(Min('timestamp'))['timestamp__min']
    diary = get_diary(info_list.exclude(timestamp=day0), day)

    wish_list = request.user.get_profile().wish_list
    raw = wish_list.raw('SELECT info_id, wl.id from accounts_userprofile_wish_list wl inner join accounts_userprofile up on wl.userprofile_id = up.id where up.id = %d order by wl.id' % (request.user.get_profile().pk))
    ordered_wish_list = []
    for i in raw:
        ordered_wish_list.append(Info.objects.get(pk=i.info_id))

    return render(request, 'wishlist/app.html', {'added':ordered_wish_list, 'title':'Lista de deseos', 'date':day, 'diary':diary, 'today':day, 'view_id':'wishlist' })


def get(request):
    if request.user.is_anonymous():
        return HttpResponse("anonymous")

    ret = list(request.user.get_profile().wish_list.values_list('uid', flat=True))
    return HttpResponse(json.dumps(ret))

@login_required
def view(request):
    day = datetime.date.today()
    info_list = Info.objects.all().order_by('-timestamp')
    day0 = Info.objects.aggregate(Min('timestamp'))['timestamp__min']
    diary = get_diary(info_list.exclude(timestamp=day0), day)

    cache_key = gen_cache_key(request)
    if cache.get(cache_key, "expired") == "expired":
        wish_list = request.user.get_profile().wish_list
        raw = wish_list.raw('SELECT info_id, wl.id from accounts_userprofile_wish_list wl inner join accounts_userprofile up on wl.userprofile_id = up.id where up.id = %d order by wl.id' % (request.user.get_profile().pk))
        ordered_wish_list = []
        for i in raw:
            ordered_wish_list.append(Info.objects.get(pk=i.info_id))

        cache.set(cache_key, ordered_wish_list)

    return render(request, 'homepage/day.html', {'added':cache.get(cache_key), 'title':'Lista de deseos', 'date':day, 'diary':diary, 'today':day, 'view_id':'wishlist' ,
        'due_dates_count': get_duedate_content()['count']
        })

