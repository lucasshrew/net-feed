#!/usr/bin/python

import os
import sys
import site

# Add the site-packages of the chosen virtualenv to work with
site.addsitedir('/home/netfeed/.virtualenvs/netfeeds/local/lib/python2.7/site-packages')

# Add the app's directory to PYTHONPATH
sys.path.insert(0, '/home/netfeed/webapp')
sys.path.insert(0, '/home/netfeed/webapp/netfeeds')

# Set django settings file
os.environ['DJANGO_SETTINGS_MODULE'] = 'netfeeds.settings.production'

# Set project needed env vars
os.environ['NF_DB_USER'] = 'netfeed_admin'
os.environ['NF_DB_PASS'] = 'M0vies4tw'

# Activate our virtual env
activate_env=os.path.expanduser("/home/netfeed/.virtualenvs/netfeeds/bin/activate_this.py")
execfile(activate_env, dict(__file__=activate_env))

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
